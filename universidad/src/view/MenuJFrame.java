package view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import controller.UniversidadController;

import java.awt.event.*;

public class MenuJFrame extends JFrame {

    // ATRIBUTOS
    private JButton btnCrear;
    private JButton btnMostrar;

    public MenuJFrame(UniversidadController uController) {
        // Añadir título
        setTitle("REGISTRO DE UNIVERSIDADES");
        setBounds(100, 100, 540, 300);
        // Indicar que debe finalizar el programa al cerrar la ventana
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        // Setear layout que viene por defecto
        getContentPane().setLayout(null);
        // Inicializar elementos
        btnCrear = new JButton("Crear universidad");
        // Dar coordenadas y tamaño al botón
        btnCrear.setBounds(20, 20, 200, 50);
        // Añadir elemento a la ventana
        add(btnCrear);

        btnMostrar = new JButton("Mostrar todas las universidades");
        btnMostrar.setBounds(230, 20, 280, 50);
        add(btnMostrar);

        // Manejadores de eventsos
        btnCrear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new UniversidadJFrame(uController);
            }
        });

        btnMostrar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Hola mundo desde un evento");
            }
        });

        // Poner la ventana visible
        setVisible(true);
    }

}
