package view;

import javax.swing.JOptionPane;

import controller.UniversidadController;

public class MenuGUI {
    // ATRIBUTOS
    private UniversidadController uController;

    // CONSTRUCTOR
    public MenuGUI(UniversidadController uController) {
        this.uController = uController;
    }

    public void crearMenu() {

        UniversidadGUI uView = new UniversidadGUI(uController);
        // Opciones del menú
        String mensaje = "-----------------UNIVERSIDADES----------------\n";
        mensaje += "1) Crear universidad\n";
        mensaje += "2) Mostrar todas las univerisdades\n";
        mensaje += "3) Consultar universidad\n";
        mensaje += "4) Actualizar universidad\n";
        mensaje += "5) Eliminar universidad\n";
        mensaje += "-1) Salir\n";
        mensaje += ">>> ";
        // Varibale que representa la opción ingresada por el usuario
        int opcion = 0;
        try {
            while (opcion != -1) {
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null, mensaje));
                // Evaluar opción
                switch (opcion) {
                    case 1:
                        uView.crearUniversidad();
                        break;
                    case 2:
                        uView.mostrarUniversidades();
                        break;
                    case 3:
                        uView.mostrarUnviersidadXnit();
                        break;
                    default:
                        break;
                }

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Ups! algo sucedió, por favor intenta mas tarde");
        }
    }

}
