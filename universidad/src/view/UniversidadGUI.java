package view;

import java.sql.ResultSet;
import java.util.Scanner;

import javax.swing.JOptionPane;

import controller.UniversidadController;

public class UniversidadGUI {
    // ATRIBUTOS
    private UniversidadController uController;

    // CONSTRUCTOR
    public UniversidadGUI(UniversidadController uController) {
        this.uController = uController;
    }

    // --INTERFAZ POR CONSOLA

    public void crearUniversidad() {
        String encabezado = "-------------CREAR UNIVERSIDAD-----------\n";
        encabezado += "Por favor ingrese la siguiente informacion\n";
        // --SOLICITAR DATOS
        String nit = JOptionPane.showInputDialog(null, encabezado + "Nit: ");
        String nombre = JOptionPane.showInputDialog(null, encabezado + "Nombre: ");
        String direccion = JOptionPane.showInputDialog(null, encabezado + "Dirección: ");
        String email = JOptionPane.showInputDialog(null, encabezado + "Email: ");
        // Crear universidad
        boolean insert = uController.crearUniversidad(nit, nombre, direccion, email);
        if (insert) {
            JOptionPane.showMessageDialog(null, "\n\nUniversidad creada con exito");
        } else {
            JOptionPane.showMessageDialog(null, "\n\nPor favor intenta mas tarde");
        }
    }

    public void mostrarUniversidades() {
        ResultSet universidades = uController.obtenerUniversidades();
        try {
            String info = "------------------UNIVERSIDADES------------------\n";
            while (universidades.next()) {
                info += universidades.getString("nombre");
                info += "\n" + universidades.getString("nit");
                info += "\n" + universidades.getString("direccion");
                info += "\n" + universidades.getString("email");
                info += "\n-----------------------------------------------------------\n";
            }
            JOptionPane.showMessageDialog(null, info);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mostrarUnviersidadXnit() {
        // Encabezado
        String info = "---------CONSULTAR UNIVERSIDAD POR NIT--------\n\n";

        String nit = JOptionPane.showInputDialog(null, info + "Nit: ");
        // Obtener la universidad
        ResultSet result = uController.consultarUniversidad(nit);
        try {
            if (result.next()) {
                info += result.getString("nombre");
                info += "\n" + result.getString("nit");
                info += "\n" + result.getString("direccion");
                info += "\n" + result.getString("email");
                info += "\n-----------------------------------------------------------\n";
                JOptionPane.showMessageDialog(null, info);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actualizarUniversidad(Scanner sc) {
        System.out.println("---------------ACTUALIZAR UNIVERSIDAD--------------");
        // --SOLICITAR DATOS
        // Solicitar nit
        System.out.println("Nit: ");
        String nit = sc.next();
        sc.nextLine();
        // Solicitar nombre
        System.out.println("Nombre: ");
        String nombre = sc.next();
        sc.nextLine();
        // Solicitar dirección
        System.out.println("Direccion: ");
        String direccion = sc.next();
        sc.nextLine();
        // Solicitar email
        System.out.println("Email: ");
        String email = sc.next();
        sc.nextLine();
        // Actualizar universidad
        boolean update = uController.actualizarUniversidad(nit, nombre, direccion, email);
        if (update) {
            System.out.println("Universidad actualizada con exito");
        } else {
            System.out.println("Por favor intenta mas tade");
        }
    }

    public void eliminarUniversidad(Scanner sc) {
        System.out.println("-----------ELIMINAR UNIVERSIDAD POR NIT-----------");
        System.out.println("Nit: ");
        String nit = sc.next();
        sc.nextLine();
        // Eliminar universidad
        boolean delete = uController.eliminarUniversidad(nit);
        if (delete) {
            System.out.println("Universidad eliminadad con exito");
        } else {
            System.out.println("Por favor intenta mas tade");
        }
    }
}
