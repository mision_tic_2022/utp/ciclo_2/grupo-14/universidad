package view;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import controller.UniversidadController;

import java.awt.event.*;

public class UniversidadJFrame extends JFrame {

    // ATRIBUTOS
    private JLabel lblNombre;
    private JLabel lblNit;
    private JLabel lblDireccion;
    private JLabel lblEmail;
    private JTextField txtNombre;
    private JTextField txtNit;
    private JTextField txtDireccion;
    private JTextField txtEmail;
    private JButton btnRegistrar;

    private UniversidadController uController;

    // CONSTRUCTOR
    public UniversidadJFrame(UniversidadController uController) {
        this.uController = uController;
        setTitle("Formulario de registro");
        getContentPane().setLayout(null);
        setBounds(100, 100, 250, 300);
        init();
        setVisible(true);
    }

    /**
     * Método para inicializar los elementos en la ventana
     */
    public void init() {
        lblNombre = new JLabel("Nombre: ");
        lblNombre.setBounds(10, 10, 80, 30);

        txtNombre = new JTextField();
        txtNombre.setBounds(81, 10, 100, 30);

        lblNit = new JLabel("Nit: ");
        lblNit.setBounds(10, 50, 60, 30);

        txtNit = new JTextField();
        txtNit.setBounds(71, 50, 100, 30);

        lblDireccion = new JLabel("Dirección: ");
        lblDireccion.setBounds(10, 90, 80, 30);

        txtDireccion = new JTextField();
        txtDireccion.setBounds(91, 90, 100, 30);

        lblEmail = new JLabel("Email: ");
        lblEmail.setBounds(10, 130, 60, 30);

        txtEmail = new JTextField();
        txtEmail.setBounds(71, 130, 100, 30);

        btnRegistrar = new JButton("Registrar");
        btnRegistrar.setBounds(71, 190, 100, 40);

        // Manejador de eventos
        btnRegistrar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                registrarUniversidad();
            }
        });

        // Añadir elementos a la ventana
        add(lblNombre);
        add(txtNombre);
        add(lblNit);
        add(txtNit);
        add(lblDireccion);
        add(txtDireccion);
        add(lblEmail);
        add(txtEmail);
        add(btnRegistrar);
    }

    public void registrarUniversidad() {
        // Obtener los datos de los campos de texto
        boolean insert = uController.crearUniversidad(txtNit.getText(), txtNombre.getText(), txtDireccion.getText(),
                txtEmail.getText());
        if (insert) {
            limpiarCampos();
            JOptionPane.showMessageDialog(this, "Universidad registrada con éxito");
        } else {
            JOptionPane.showMessageDialog(this, "Ups! algo sucedió por favor intenta mas tarde");
        }
    }

    public void limpiarCampos() {
        txtNombre.setText("");
        txtNit.setText("");
        txtDireccion.setText("");
        txtEmail.setText("");
    }

}
